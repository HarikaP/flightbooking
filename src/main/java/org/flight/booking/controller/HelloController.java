package org.flight.booking.controller;

import org.flight.booking.services.FlightSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HelloController {


    @Autowired
    FlightSearchService service ;
    @RequestMapping("/flight/search")
    public ResponseEntity<List<String>> returnFlightsWithPassengers(@RequestParam("origin") String origin, @RequestParam("destination") String destination, @RequestParam("noOfPassengers") String noOfPassengers)
    {

        List<String> list= service.returnFlights(origin,destination,noOfPassengers);
        return ResponseEntity.ok(list);
    }
//    @GetMapping("/flight/search")
//    @ResponseBody
//    public List<String> returnFlightswithoutNoOfPassengers(@RequestParam("origin") String origin, @RequestParam("destination") String destination){
//        return service.returnFlights(origin,destination,null);
//    }

}
