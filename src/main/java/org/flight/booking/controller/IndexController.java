package org.flight.booking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @GetMapping("/flights")
    public String index() {
        return "FlightBooking";
    }

    //@GetMapping("/ http://localhost:63343/FlightTicketbooking/flightbookingportal/templates/FlightBooking.html?_ijt=75e0nn4f2sr8uok03g9mjivcjs")
}

