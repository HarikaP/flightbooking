package org.flight.booking.models;

import java.util.List;

public class AjaxResponseBody {
    private String msg;
    private List<FlightDetails> flights;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }



    public List<FlightDetails> getFlights() {
        return flights;
    }

    public void setFlights(List<FlightDetails> flights) {
        this.flights = flights;
    }
}
