package org.flight.booking.models;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


public class Booking {
    private String origin;
    private String destination;
    private String noOfPassengers;


    public Booking(String origin, String destination, String noOfPassengers){
        this.origin = origin;
        this.destination = destination;
        this.noOfPassengers = noOfPassengers;
    }



    public String getDestination() {
        return destination;
    }

    public String getNoOfPassengers() {
        return noOfPassengers;
    }

    public String getOrigin() {
        return origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNoOfPassengers(String noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
