package org.flight.booking.models;

import java.util.Date;

public class FlightDetails {
    /*private String flightNo;*/
    private String flightName;
    private String origin;
    private String destination;
    private String noOfPassengers;
    private Date departureDate;

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    private Date returnDate;

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    private int maxSeats;
/*
    public FlightDetails(String origin, String destination, String noOfPassengers){
        this.origin = origin;
        this.destination = destination;
        this.noOfPassengers = noOfPassengers;
        //this.flightNo = flightNo;
       // this.departureDate = departureDate;
    }*/


    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getDestination() {
        return destination;
    }

    public String getNoOfPassengers() {
        return noOfPassengers;
    }

    public String getOrigin() {
        return origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNoOfPassengers(String noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }


    public String getFlightName() {
        return flightName;
    }

   /* public String getFlightNo() {
        return flightNo;
    }
*/
    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

   /* public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }*/

}

