package org.flight.booking.repository;

public interface CustomRepository<T> {
    public void store(T t);
    public T retrieve();

}
