$(document).ready(function () {

    $("#flights").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var data = {}
    data["source"] = $("#source").val();
    data["destination"] = $("#destination").val();
    data["noOfPassengers"] = $("#noOfPassengers").val();

    $("#search").prop("disabled", true);
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/flight/search",
        data:{
            origin:$("#source").val(),
            destination:$("#destination").val(),
            noOfPassengers:$("#noOfPassengers").val()
        },
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#list').html(json);

            console.log("SUCCESS : ", data);
            $("#search").prop("disabled", false);

        }
      /*  error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#list').html(json);

            console.log("ERROR : ", e);
            $("#Search").prop("disabled", false);

        }*/
    });

}
