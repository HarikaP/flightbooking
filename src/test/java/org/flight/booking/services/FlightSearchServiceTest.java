package org.flight.booking.services;


import org.flight.booking.repository.FlightRepository;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FlightSearchServiceTest {


    public List<String> mockedList(){
        List<String> expectedList  = new ArrayList<String>();
        expectedList.add("Boeing 777-200LR(77L)");
        expectedList.add("Airbus A319 V2");
        expectedList.add("Airbus A321");

        return expectedList;
    }
    @Mock
    private FlightRepository flightRepository;

    @Test
  public void returnEmptyListifNoFlights(){
        List expectedList = new ArrayList();
        FlightRepository flightRepository  = mock(FlightRepository.class);
        when(flightRepository.retrieve()).thenReturn(expectedList);
        FlightSearchService search = new FlightSearchService();
        List<String> result = search.returnFlights("Hyderbad","Delhi","3");
        assertEquals(flightRepository.retrieve().size(), result.size());

  }

    @Test
    public void test()
    {
        List expectedList = mockedList();
        FlightRepository flightRepository  = mock(FlightRepository.class);
        when(flightRepository.retrieve()).thenReturn(expectedList);
        FlightSearchService search = new FlightSearchService();
        List<String> result = search.returnFlights("Hyderabad","Delhi","3");
        assertEquals(flightRepository.retrieve().size(), result.size());


    }
    @Test
    public void test1()
    {
        List expectedList = mockedList();
        FlightRepository flightRepository  = mock(FlightRepository.class);
        when(flightRepository.retrieve()).thenReturn(expectedList);
        FlightSearchService search = new FlightSearchService();

        List<String> result = search.returnFlights("Hyderabad","Delhi","0");
        assertEquals(flightRepository.retrieve().size(), result.size());

    }

/*

    @Test
    public void returnFlightList() {
        FlightSearchService search = new FlightSearchService();
        FlightRepository rep = new FlightRepository();
        List<String> result = search.returnFlights("Hyderabad","Delhi","3");
        assertEquals(rep.retrieve(), result);
    }*/

}